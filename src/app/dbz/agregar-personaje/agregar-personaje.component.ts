import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { Personaje } from '../Interfaces/dbz.interface';
import { DbzService } from '../services/dbz.service';

@Component({
  selector: 'app-agregar-personaje',
  templateUrl: './agregar-personaje.component.html',
  styleUrls: ['./agregar-personaje.component.sass']
})
export class AgregarPersonajeComponent implements OnInit {

  @Input() nuevo_personaje: Personaje = {
    nombre: '',
    poder: 0
  };

  // This event emits values to the parent
  // @Output() onAgregarPersonaje = new EventEmitter<Personaje>();

  constructor(
    private dbzService: DbzService
  ) { }

  ngOnInit(): void {
  }

  agregar() {
    if (this.nuevo_personaje.nombre.trim().length === 0) { return; } // if name is empty then doing nothing

    // this.onAgregarPersonaje.emit(this.nuevo_personaje);
    this.dbzService.agregarPersonaje(this.nuevo_personaje);
    this.nuevo_personaje = {
      nombre: '',
      poder: 0
    }
  }
}
