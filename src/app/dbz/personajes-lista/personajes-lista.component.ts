import { Component, Input, OnInit } from '@angular/core';

import { Personaje } from '../Interfaces/dbz.interface';
import { DbzService } from '../services/dbz.service';

@Component({
  selector: 'app-personajes-lista',
  templateUrl: './personajes-lista.component.html',
  styleUrls: ['./personajes-lista.component.sass']
})
export class PersonajesListaComponent implements OnInit {

  // Inyectamos el servicio necesario
  // Recordemos que Angular utiliza el patrón Singleton
  constructor(
    private dbzService: DbzService
  ) { }

  ngOnInit(): void {
  }

  get personajes() {
    return this.dbzService.personajes;
  }
}
