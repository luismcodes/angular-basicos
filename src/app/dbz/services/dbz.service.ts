import { Injectable } from '@angular/core'; // Singleton pattern

import { Personaje } from '../Interfaces/dbz.interface';

@Injectable({
  providedIn: 'root'
})
export class DbzService {

  // Agregagos '_' en el nombre de la variable para indicar que es privada; solo es un estándar y buena práctica
  private _personajes: Personaje[] = [
    {
      nombre: 'Goku',
      poder: 150000
    },
    {
      nombre: 'Vegeta',
      poder: 100000
    },
    {
      nombre: 'Trunks',
      poder: 30000
    }
  ];

  constructor() { }

  // Método para obtener array de personajes
  get personajes(): Personaje[] {
    return [...this._personajes]; // Utilziamos [...] porque angular retorna con referencia.
  }

  agregarPersonaje(personaje: Personaje) {
    this._personajes.push(personaje);
  }
}
