import { Component, OnInit } from '@angular/core';

import { Personaje } from '../Interfaces/dbz.interface';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.sass']
})
export class MainPageComponent implements OnInit {

  // Vars
  nuevo: Personaje = {
    nombre: '',
    poder: 0
  };

  constructor() { }

  ngOnInit(): void {
  }
}
