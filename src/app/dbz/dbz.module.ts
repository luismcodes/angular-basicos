import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page/main-page.component';
import { FormsModule } from '@angular/forms';
import { PersonajesListaComponent } from './personajes-lista/personajes-lista.component';
import { AgregarPersonajeComponent } from './agregar-personaje/agregar-personaje.component';



@NgModule({
  declarations: [
    MainPageComponent,
    PersonajesListaComponent,
    AgregarPersonajeComponent
  ],
  exports: [
    MainPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class DbzModule { }
